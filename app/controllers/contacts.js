import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
    store: service(),
    name: '',
    email: '',
    actions: {
        get() {
            this.store.findAll('contact').then((data) => {
                data.json();
                console.log('data:', data);
                // return data;
            })
        },
        createContact() {
            let {name , email} = this;
            let contactDetails = {
                name,
                email
            };
            console.log('contact created!');
            this.store.createRecord('contact', contactDetails);
            this.set('contactModel', contactModel);
            let response = yield contactModel.saveRecord();
            console.log(response);
            return response;

        }
    }
})
