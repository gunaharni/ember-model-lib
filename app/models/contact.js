import Resource from 'ember-model-library/models/common/resource';

let ContactModel = Resource.extend({
  resourceUrl: '/contacts',
  resourceProperties: ['name', 'email']

});

ContactModel.reopenClass({
  responsePath: 'contacts',
});

export default ContactModel;
